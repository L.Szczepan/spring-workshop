package com.craftincode.springworkshop.repository;

import com.craftincode.springworkshop.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, String>{
    List<User> findAll();
}
