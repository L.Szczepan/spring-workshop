# Spring workshop

Tutorial stworzony przez szkołę programowania [Craftin' Code](http://www.craftincode.com)

Poniższy tutorial jest "sprintem" przez Spring framework poruszającym większość najciekawszych
i najpopularniejszych elementów Springa. Jest to wyłącznie uproszczona prezentacja możliwości. Jeśli 
chcesz poznać Springa oraz inne technologie używane w tworzeniu aplikacji biznesowych w Javie 
(JEE, Spring, JPA, Hibernate, JSP, MVC) dokładnie, krok po kroku zapraszamy na nasz kurs 
stacjonarny - [Ekspresowy Kurs Java - Poziom Zaawansowany](http://www.craftincode.com/ekspresowy-kurs-java-zaawansowany-wroclaw/)

Zapraszamy również do polubienia naszego [fanpage'a na Facebooku](https://www.facebook.com/craftincode/) 
gdzie na bieżąco publikujemy informacje na temat naszych kursów, wydarzeń oraz przydatne informacje wszystkim, którzy
uczą się programowania.


## 1. Wygenerowanie projektu
Na stronie [https://start.spring.io/](https://start.spring.io/) wygeneruj projekt podając dowolne `groupId` i `artifactId`.
W zależnościach wyszukaj oraz dodaj zależność `Web`.

Po naciśnięciu `Generate Project` zostanie pobrane archiwum *.zip z gotowym projektem. Należy go rozpakować oraz zaimportować
w IntelliJ (najprościej otworzyć z poziomu IntelliJ plik `pom.xml`).

Gdy projekt zostanie poprawnie zaimportowany można uruchomić aplikację korzystając z metody `main` w klasie `TwojArftifactIdApplication` (np. `SpringWorkshopApplication`).

Jeśli serwer się poprawnie odpalił pod adresem [http://localhost:8080/](http://localhost:8080/) powinien pojawić się komunikat `Whitelabel Error Page`

## 2. Hello controller
Stwórz pakiet `controllers` a w nim klasę `HelloController` o zawartości:

```java
@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }

    @GetMapping("/hello/{username}")
    public String helloWithName(@PathVariable String username) {
        return "Hello user: " + username;
    }
}
```

Teraz Twój serwer potrafi odpowiadać! Po restarcie serwera możesz zapytać go pod poniższymi adresami:

[http://localhost:8080/hello](http://localhost:8080/hello)

[http://localhost:8080/hello/JanKowalski](http://localhost:8080/hello/JanKowalski)

[http://localhost:8080/helloWithParam?firstName=Adam&lastName=Miauczyński](http://localhost:8080/helloWithParam?firstName=Adam&lastName=Miauczy%C5%84ski)

## 3. Stworzenie modelu użytkownika
Stwórz pakiet `model` a w nim klasę `User` służącą do reprezentacji danych użytkowników przechowywanych w naszej aplikacji. Klasa ta powinna 
posiadać 2 - `String login` oraz `String password`. Stwórz/wygeneruj konstruktor bezparametrowy, konstruktor inicjalizujący
oba pola, "gettery" oraz "settery" oraz dla wygody metodę `toString`

Klasa powinna wyglądać jak poniżej:

```java
public class User {
    private String login;
    private String password;

    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
```

## 4. User controller + service
### UserService
Stwórz pakiet `service` a w nim klasę `UserService`:

```java
@Service
public class UsersService {

    private List<User> users = new ArrayList<>();

    @PostConstruct
    public void init() {
        users.add(new User("admin", "admin"));
        users.add(new User("user", "123"));
    }

    public List<User> getAllUsers() {
        return users;
    }

    public void createUser(User user) {
        users.add(user);
    }
}
```

Jest to "serwis" udostępniający operacje na naszej liście użytkowników.


### UsersController
Stwórz w pakiecie `controllers` klasę `UserController`:

```java

@RequestMapping("/api/v1/users")
@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping
    public List<User> getAllUsers() {
        return usersService.getAllUsers();
    }

    @PostMapping
    public void createUser(@RequestBody User user) {
        usersService.createUser(user);
    }
}
```
Teraz pod adresem [http://localhost:8080/api/v1/users](http://localhost:8080/api/v1/users) Twój serwer zwraca listę
użytkowników w formacie JSON.

Wysyłając żądanie typu post (w ciele podając w formacie JSON dane użytkownika) pod ten sam adres możemy stworzyć użytkownika.

`Uwaga`: Pamiętaj aby ustawić nagłówek `Content-Type` o wartości `application/json`

Przykładowe ciało zapytania typu POST:

```json
{
    "login": "jan",
    "password": "abc123"
}
```

## 4.5  Swagger

Dodaj do pliku `pom.xml` zależności

```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
```
Oraz nad główną klasą aplikacji ("ta z metodą `main`") adnotację `@EnableSwagger2`:

```java
@EnableSwagger2
@SpringBootApplication
public class SpringWorkshopApplication {
    // ... 
}
```

Od tej pory pod adresem [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) jest dostępny *Swagger* - dokumentacja oraz "tester" API Twojej aplikacji.


## 5. Spring MVC - homepage
Dodaj do pliku `pom.xml` zależność
```java
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```
Od tej chwili Twoja aplikacja może poprawnie obsługiwać wyświetlanie szablonów w technologii `Thymeleaf`

Stwórz w folderze `/src/main/resources` folder `templates` a w nim plik HTML `home.html`

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <title>Spring Workshop</title>
    </head>
    <body>
        <h1>Hello Spring!</h1>
    </body>
</html>
```

Stwórz w projekcie nowy pakiet `view` a w nim klasę `ViewController`:

```java
@Controller
public class ViewController {

    @Autowired
    private UsersService usersService;

    @GetMapping("/")
    public ModelAndView homePage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        return modelAndView;
    }
}
```

## 6. Spring MVC + Thymeleaf - lista użytkowników
Dodaj do folderu `templates` z poprzedniego zadania nowy plik `users.html`
```java
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8"/>
        <title>Użytkownicy</title>
    </head>
    <body>
        <h1>Lista użytkowników</h1>

        <table>
            <thead>
                <th>
                    Login
                </th>
                <th>
                    Hasło
                </th>
            </thead>

            <tbody>
                <th:block th:each="user : ${users}">
                    <tr>
                        <td th:text="${user.login}">...</td>
                        <td th:text="${user.password}">...</td>
                    </tr>
                </th:block>
            </tbody>
        </table>
    </body>
</html>
```

W pliku `home.html` dodaj hiperłącze do adresu `/users`:
```html
<a href="/users">Lista użytkowników</a>
```

Do `ViewController` dodaj pole 
```java
@Autowired
private UsersService usersService;
```
oraz metodę

```java
@GetMapping("/users")
public ModelAndView users(){
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("users");
    modelAndView.addObject("users", usersService.getAllUsers());
    return modelAndView;
}
```

Od tej chwili pod adresem [http://localhost:8080/users](http://localhost:8080/users) 
istnieje strona wyświetląca tabelę z danymi wszystkich użytkowników.

## 7. Spring security
Dodaj do pliku `pom.xml` zależność:
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

Od tej chwili Twoja aplikacja jest zabezpieczona (wymaga zalogowania). Nie jest to jednak
połączone jeszcze z przechowywanymi przez nas użytkonikami dlatego mamy do dyspozycji
konto domyślnie stworzone przez Spring Security:

- Nazwa użytkownika: `user`
- Hasło: generowane losowo przez Springa przy uruchomieniu aplikacji, jest wyświetlane w logach (konsoli) w linii `Using default security password:`

## 8. Uruchamiamy naszą aplikację na prawdziwym serwerze:

`Uwaga: Serwer użyty w tym punkcie jest utworzony tylko na czas trwania warsztatów, jeśli chcesz ten punkt odtworzyć samodzielnie
w domu możesz skontaktować się z nami a udzielimy Ci porad jak wykupić i skonfigurować własny serwer`

Zmień rozszerzenie pliku `application.properties` na `*.yml` (znajduje się on w `src/main/resources`) -> `application.yml`. 
Dodaj w nim wpis `server.port: 8001`. Podana wartość to numer portu na jakim uruchomi się aplikacja 
(podczas warsztatów trener poda Ci indywidualny numer portu).

Wykonaj `mvn package` (z konsoli lub korzystając z IntelliJ). W folderze `target` powinien zostać wygenerowany plik `JAR` 
z Twoją aplikacją.

W programie Filezilla połącz się do serwera:
- Host: `sftp://142.93.166.162`
- Username: `root`
- Password: `craftincode_wro01`

Utwórz folder (w moim przypadku `wrabel`)dla swojej aplikacji i wgraj tam wygenerowanego wcześniej JAR-a.

Połącz się za pomocę SSH do serwera - np. z użyciem konsoli Git Bash (hasło `craftincode_wro01`):

```ssh root@142.93.166.162```

Wejdź do swojego folderu:
```cd wrabel```

Uruchom swojego JAR-a:
```java -jar spring-workshop-0.0.1-SNAPSHOT.jar```

Brawo! Teraz pod adresem (port zależy od tego jaki ustawiłeś w `application.yml`) [142.93.166.162:8080]([142.93.166.162:8080]) znajduje się Twoja aplikacja!


## 9. Spring JPA - konfiguracja
Dodaj w pliku `pom.xml` zależność:

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>

<dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <scope>runtime</scope>
</dependency>
```

W pliku `application.yml` dodaj konfigurację:

```yaml
spring:
  datasource.url: jdbc:h2:file:./h2db;DB_CLOSE_DELAY=-1
  jpa.hibernate.ddl-auto: update
```
W tutorialu użyjemy prostej bazy plikowej H2. W celu użycia innej bazy wystarczy jedynie zmienić
powyższą konfigurację.


Zmodyfikuj klasę `User` dodając nad klasą adnotację `@Entity` oraz nad polem `login` adnotację `@Id`

```java
@Entity
public class User {
    @Id
    private String login;
    // .. reszta klasy
``` 

W tej chwili aplikacja po uruchomieniu utworzy bazę danych oraz automatycznie utworzy tabelę
do przechowywania użytkowników!

## 9. Spring JPA - repozytorium
Stwórz pakiet `repository` a w nim klasę `UserRepository`:

```java
@Repository
public interface UserRepository extends CrudRepository<User, String>{
    List<User> findAll();
}
```

Następnie zmodyfikuj `UserService` tak aby korzystał z utworzonego repozytorium:

```java
@Service
public class UsersService {
    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init() {
        if (userRepository.findOne("admin") == null) {
            userRepository.save(new User("admin", "admin"));
        }
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void createUser(User user) {
        userRepository.save(user);
    }
}
```

W tej chwili Twoja aplikacja przechowuje wszystkie dane na temat użytkoników w bazie danych.

## 10. Spring Security - korzystanie z naszej bazy użytkowników

Zmodyfikuj klasę `UserService` tak aby implementowała interfejs `UserDetailsService` oraz dodaj 
do niej następującą metodę:

```java
@Override
public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    User user = userRepository.findOne(login);
    if (user == null) {
        throw new UsernameNotFoundException("Nie istnieje użytkownik o loginie " + login);
    } else {
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), new ArrayList<>());
    }
}
```

Stwórz pakiet `config` a następnie stwórz w nim klasę `SecurityConfig`:

```java
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private UserDetailsService myUserDetails;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(myUserDetails);
        auth.authenticationProvider(authenticationProvider);
    }
}
```

W tej chwili Spring do autoryzacji/autentykacji/logowania używa naszej bazy użytkowników.


## 11. Dodanie formularza rejestracji użytkownika

Dodaj w pliku `home.html` hiperłącze do strony `/register`
```html
<a href="/register">Zarejestruj użytkownika</a>
```

Stwórz w folderze `templates` nowy szablon - `register.html` (formularz rejestracji)
```html
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8"/>
        <title>Użytkownicy</title>
    </head>
    <body>
        <h1>Rejestracja użytkownika</h1>

        <form action="#" th:action="@{/register}" th:object="${user}" method="post">
            Login:
            <input type="text" th:field="*{login}"/>
            Password:
            <input type="text" th:field="*{password}"/>
            <input type="submit" value="Stwórz użytkownika"/>
        </form>
    </body>
</html>
```

Stwórz w folderze `templates` nowy szablon - `registrationFinished.html` (strona wyświetlana po rejestracji)
```html
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8"/>
        <title>Użytkownicy</title>
    </head>
    <body>
        <h1>Zarejestrowano użytkownika!</h1>

        <a href="/">Powrót do strony głównej</a>
    </body>
</html>
```

Dodaj w `ViewController` 2 nowe metody:
```java
@GetMapping("/register")
public ModelAndView register(){
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("register");
    modelAndView.addObject("user", new User());
    return modelAndView;
}

@PostMapping("/register")
public ModelAndView registerPost(@ModelAttribute User user) {
    System.out.println("Rejestruję użytkownika: " + user);
    usersService.createUser(user);
    return new ModelAndView("registrationFinished");
}
```

W tej chwili Twoja aplikacja posiada funkcjonalnosć rejestracji nowego użytkownika pod
adresem [http://localhost:8080/register](http://localhost:8080/register) (lub z hiperłącza na stronie głównej)
